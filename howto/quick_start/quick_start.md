# 快速开始指导

该指导文档旨在提供如下帮助：
- 在Ubuntu上配置TenonOS开发环境
- 获取TenonOS源码，创建helloworld工程
- 编译构建出app-helloworld镜像，在kvm/raspberry-pi-3b+开发板上运行

本文档使用：
- Ubuntu 22.04
- Raspberry Pi 3 Model B+开发板
- SmarTTY
- CMSIS-DAP调试器: 国产muselab调试器(用作串口线)

  调试器接口明细见[debug.md][debug-base]


### 环境安装
1. 更新包管理器
```
sudo apt update
sudo apt upgrade
```
2. 下载依赖组件
```
sudo apt install git make build-essential libncurses5-dev \
  libncursesw5-dev flex bison gcc-aarch64-linux-gnu \
  qemu-system-arm qemu-efi qemu jq device-tree-compiler
```
### kvm镜像的编译构建及运行

##### tenon + app-helloworld源码获取
```
mkdir my_TenonOS
cd my_TenonOS
git clone https://gitee.com/tenonos/tenon
mkdir apps
cd apps
git clone https://gitee.com/tenonos-mirror/app-helloworld.git
```

创建完成的工程结构如下
```
my_TenonOS
├── apps
│   ├── app-helloworld
└── tenon                 
    ├── arch                     
    ├── drivers         
    ├── include          
    ├── lib               
    ├── plat               
    └── support            
```

本示例中将拉取tenon及apps的工作目录my_TenonOS保存为环境变量TENONOS_DIR
```
export TENONOS_DIR=/path/to/your/my_TenonOS
```
进入tenon目录下，使用指令进入内核配置阶段，
```
make menuconfig A=$TENONOS_DIR/apps/app-helloworld
```

##### kvm config配置
```
- 选择：Architecture Selection  ---> Architecture (x86 compatible (64 bits))  --->Armv8 compatible (64 bits)  
- 选择：Architecture Selection  ---> Target Processor (Generic Armv8 CPU)  --->  Generic Armv8 CPU
- 取消选择：Architecture Selection  ---> Workaround for Cortex-A73 erratum 858921
- 选择：Platform Configuration  ---> KVM guest
```

在同目录下执行如下指令，
```
make A=$TENONOS_DIR/apps/app-helloworld
```
编译后，会在$TENONOS_DIR/apps/app-helloworld目录下生成build目录，其中包括kvm镜像app-helloworld_qemu-arm64

进入该build目录下执行
```
sudo qemu-system-aarch64 -machine virt -cpu cortex-a53 -m 4G -nographic -kernel app-helloworld_qemu-arm64
```
串口打印结果如下，

<img src="./img/serial_output.png" alt="cmsis-dap" width="280" height="200"/>


### raspi镜像的编译构建及运行

##### board-support-package源码获取

生成可在raspi3b+开发板运行的helloworld镜像需要使用bsp仓库，

```
  cd $TENONOS_DIR
  git clone https://gitee.com/tenonos/board-support-package
  cd board-support-package
  chmod +x *.sh
  ./build.sh --init-libs=$TENONOS_DIR/board-support-package/lib_config.json
```

执行完成后的工程结构如下，代码结构详解见[tenon][tenon-home]
```
my_TenonOS
├── apps
│   ├── app-helloworld
├── libs
├── drivers
├── board-support-package  
│   ├── kvm
│   └── raspberry-pi-3b+
├── plats
│   ├── plat-raspi
└── tenon                 
    ├── arch            
    ├── build          
    ├── drivers         
    ├── include          
    ├── lib               
    ├── plat               
    └── support            
```

接着使用.sh脚本构建raspi镜像，追加参数
- -k 内核tenon路径
- -b board名称
- -c raspi默认配置文件
- -a 应用路径

```
./build.sh -k=$TENONOS_DIR/tenon -b=raspberry-pi-3b+ -c=$TENONOS_DIR/tenon-board-support-package/raspberry-pi-3b+/defconfig -a=$TENONOS_DIR/apps/app-helloworld
```

生成的tar包所在目录 $TENONOS_DIR/tenon-board-support-package/raspberry-pi-3b+/build，解压该tar包，获取镜像文件

```
tar -xzvf Tenon-[version].tar.gz
```

解压后该目录下包含如下内容
- <span style='color:cyan;'>bcm2710-rpi-3-b-plus.dtb</span>  
- KERNEL_README.md
- BOARD_README.md           
- Tenon-[version].tar.gz
- <span style='color:cyan;'>config.txt</span>
- <span style='color:cyan;'>fixup.dat</span> 
- <span style='color:cyan;'>start.elf</span> 
- <span style='color:cyan;'>bootcode.bin</span> 
- <span style='color:cyan;'>kernel8.img</span> 

将解压后获取的的bootcode.bin 、config.txt 、fixup.dat 、start.elf、kernel.8.img五个文件以及bcm2710-rpi-3-b-plus.dtb复制到sd卡中

- 接通串口线

<img src="./img/raspi_TX_RX.png" alt="cmsis-dap" width="280" height="200"/> 

注：树莓派与DAP仿真器的TX和RX需交叉接线，接线示意图：

<img src="./img/raspi_dap_connection.png" alt="cmsis-dap" width="280" height="160"/> 

- 插入树莓派的sd卡槽中上电

<img src="./img/raspi_sd.png" alt="cmsis-dap" width="280" height="200"/> 

- 串口输出内容如下，

<img src="./img/raspi_app_output.png" alt="cmsis-dap" width="280" height="200"/> 

更详细的各平台镜像生成指导请参考[BoardSupportPackage][BoardSupportPackage-home]

[debug-base]: https://gitee.com/tenonos/documents/howto/debug.md
[tenon-home]: https://gitee.com/tenonos/tenon
[BoardSupportPackage-home]: https://gitee.com/tenonos/board-support-package
