# debug调试指导

该指导文档旨在帮助开发人员快速上手搭载TenonOS的板卡调试工作，下介绍debug过程中常用软硬件工具，并辅以kvm及raspi3b+开发板实际调试场景作说明。
在阅读本文前，建议您先阅读[quick_start][quick_start-base]，以对TenonOS架构有初步的认识，同时可获取可执行的镜像及调试用的二进制文件。

本文档使用：
- Ubuntu 22.04
- OpenOCD 0.12.0
- GDB: gdb-multiarch
- Raspberry Pi 3 Model B+开发板
- CMSIS-DAP调试器: 国产muselab调试器

<img src="./img/cmsis-dap_front.png" alt="cmsis-dap_front" width="200"/>

<img src="./img/cmsis-dap_back.png" alt="cmsis-dap_back" width="250" height="148"/>

muselab调试器引脚说明

| 引脚名称  | 功能说明           |
| ---- | --------------------------------- |
| TDI  | test data input, jtag接口           |
| TCK  | test clock, jtag接口                |
| TDO  | test data output, jtag接口           |
| GND  | 接地                                |
| nRST | reset，可用于jtag的TRST，作为jtag的reset信号 |
| TMS  | test mode select, jtag接口          |
| TX   | 串口发送      |
| RX   | 串口接收       |

注：dap调试器串口及debug调试功能互不干扰。对于串口功能，需使用TX/RX/GND引脚(与树莓派3b+开发板的连接方式详见[quick_start][quick_start-base])；
对于debug调试功能 需连接TDI/TCK/TDO/TMS/GND引脚，具体连线方式见下文树莓派调试实例；
树莓派由电源适配器单独供电，调试器上3v3/5v不与树莓派开发板连接。


#### GDB安装
本文档使用gdb-multiarch，该GDB版本支持多种架构，如x86, ARM, AArch64，安装指令如下，
```
sudo apt-get install gdb-multiarch
```
#### OpenOCD安装
OpenOCD用于建立Ubuntu与被测单板之间的jtag连接，同时启动GDB-Server/Telnet，用于下发单步调试、寄存器查询等命令。

OpenOCD安装0.12.0版本
本文使用CMSIS-DAP调试器，故line6配置启用cmsis-dap adapter driver，用户可根据调试设备自行适配。
```
apt install libtool pkg-config libusb-1.0-0-dev libhidapi-dev
git clone https://git.code.sf.net/p/openocd/code openocd   
cd openocd
git checkout v0.12.0
./bootstrap
./configure --prefix=/opt/openocd --enable-maintainer-mode --enable-cmsis-dap --enable-target64
make
sudo make install
sudo apt install openocd
```
##### OpenOCD配置
若在Ubuntu下采用上述源码编译的方式安装OpenOCD，用户可在/openocd/tcl/目录下可以找到预置的板卡、芯片、cpu相关的配置文件，可作为修改模板。

##### .cfg文件分类

/openocd/tcl/目录下有如下子目录，存放不同.cfg文件，

| 子目录       | 用途                                                                                                  |
|:---------:| --------------------------------------------------------------------------------------------------- |
| interface | 保存adapter的相关配置，如jlink.cfg，定义使用的adapter driver, transport协议 (jtag 、swd等)                             |
| board     | 保存具体板卡相关配置，比如rpi3.cfg、mini2440.cfg等，一般会引用target(目标芯片)的配置，额外添加板卡外设相关配置，如SDRAM、external flash、可用地址信息等 |
| target    | 保存目标芯片相关配置，如树莓派使用的bcm2837.cfg、bcm2835.cfg                                                           |

#### raspi3b+ .cfg文件适配

实际使用OpenOCD调试树莓派开发板需对原本的.cfg文件作部分修改，修改如下，

- interface/cmsis-dap.cfg

   本文档使用jtag接口与目标通讯，此处选择jtag
```
# SPDX-License-Identifier: GPL-2.0-or-later

#
# ARM CMSIS-DAP compliant adapter
#
# http://www.keil.com/support/man/docs/dapdebug/
#

adapter driver cmsis-dap
transport select jtag

# Optionally specify the serial number of CMSIS-DAP usb device.
# adapter serial 02200201E6661E601B98E3B9
  ```
- target/bcm2837.cfg

   将adapter speed设置为200000KHz。该值为经验值，一般设置为arm时钟的1/6。根据[树莓派官方文档](https://www.raspberrypi.com/documentation/computers/config_txt.html#overclocking)，arm核心的时钟频率可以通过arm_freq设置(config.txt)，未设置时，raspi3b+默认时钟频率为1400MHz，其1/6约为200MHz。
```
......
jtag newtap $_CHIPNAME cpu -expected-id $_DAP_TAPID -irlen 4
adapter speed 200000

dap create $_CHIPNAME.dap -chain-position $_CHIPNAME.cpu
......
```

## raspi + openocd + gdb 调试实例

### 硬件连接

| 管脚名      | GPIO编号 | mode | GPIO扩展口编号(树莓派插槽编号) | cmsis-dap管脚名 |
| -------- | ------ | ---- | ------------------ | ------------ |
| ARM_TMS  | 27     | ALT4 | 13                 | TMS/IO       |
| ARM_TCK  | 25     | ALT4 | 22                 | TCK/CK       |
| ARM_TDI  | 4      | ALT5 | 7                  | TDI          |
| ARM_TDO  | 24     | ALT4 | 18                 | TDO          |
| ARM_TRST | 22     | ALT4 | 15                 | nRST         |


<img src="./img/raspi_gpio.png" alt="cmsis-dap" width="200"/>   

注：
- 表中GPIO拓展口编号，对应raspi开放的GPIO插槽，插槽编号与GPIO编号并不相同。

- 树莓派GPIO需通过config.txt配置模式，config.txt需增加如下两行，将GPIO4号管脚设置成ALT5模式，GPIO22-27号管脚设置成ALT4模式。

   ```
    gpio=4=a5
    gpio=22-27=a4
   ```

### 软件调试
注：由于openocd需要在开发板启动后，才能通过jtag口连接设备，而树莓派启动后会直接启动kernel镜像。为正常使用gdb给内核打断点，需要在kernel启动后立即停止，故修改树莓派的启动代码start.S(目录board-support-package/raspberry-pi-3b+/start.S)，

   ```
   ......
   .global _start
   _start:
      b    _start
   ......
   ```
   这样树莓派在进入到内核后，会在第一步阻塞住，方便调试。

1. 后台启动openocd，日志重定向到/opt/openocd/openocd.cfg(可选)
   
   ```
   openocd -f interface/cmsis-dap.cfg -f board/rpi3.cfg > /opt/openocd/openocd.log 2>&1 &
   ```

2. target remote

   在另一终端中，连接GDB-Server，指令如下，其中用于调试的.elf二进制文件随镜像一并生成

   ```
   gdb-multiarch tenon_raspi-arm64.elf
   ``` 

   使用target remote，连接远程端口，树莓派cpu0端口号为3333，

   ```
   target remote: 3333
   ```

   在与cpu0创建连接后，可看到cpu0阻塞在b _start的循环中，此时键入如下指令，修改pc指针的值，即可让cpu0跳出该循环，完成后续的调试，

   ```
   set $pc = 0x80004
   ```

   GDB常用指令可参考[GDB常用指令](https://ethanol1310.github.io/categories/Tutorial/Code-debugging-with-GDB/)。

3. 关闭openocd进程
   
   ```
   kill 'pidof openocd'
   ```
   
## KVM gdb调试
   
   kvm 由于不需要物理的调试器，可直接通过gdb调试，过程如下

1. 运行镜像，指令如下

   ```
   sudo qemu-system-aarch64 -machine virt -cpu cortex-a53 -m 4G -nographic -kernel tenon_qemu-arm64 -s -S
   ```
   较之普通的启动镜像指令，-s -S选项说明，
   ```
   -s 启动一个默认的 GDB-Server，监听在TCP端口1234上
   -S 在启动后暂停cpu
   ```

2. target remote

   在另一终端中，连接GDB-Server，指令如下，其中用于调试的.dbg二进制文件随镜像一并生成

   ```
   gdb-multiarch tenon_qemu-arm64.dbg
   ``` 

   <img src="./img/aarch64_gdb_connect.png" alt="aarch64_gdb_connect" width="200"/>

   使用target remote，连接远程端口，

   ```
   target remote: 1234
   ``` 
   出现如下界面，即可继续单步调试，

   <img src="./img/aarch64_gdb_connect_result.png" alt="aarch64_gdb_connect_result" width="200"/>

[quick_start-base]: https://gitee.com/tenonos/documents/howto/quick_start.md
