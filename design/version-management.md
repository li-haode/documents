# TenonOS 版本管理
## 版本号
TenonOS采用[SemVer2.0.0](https://semver.org/lang/zh-CN/)语义化版本规范

**版本格式：release-vMAJOR.MINOR.PATCH，版本号递增规则如下**

```
MAJOR：API版本变更时，递增MAJOR
MINOR：API版本兼容，新增功能性，递增MINOR
PATCH：无API、特性变更，增加bugfix，递增PATCH
```
当前TenonOS处于初始开发状态，版本号为release-v0.x.y，在release-v1.0.0发布之前，API可能发生不兼容变更。

## TenonOS版本和微库版本

当前TenonOS社区存在两类版本号：
- 各仓库独立维护的tag，格式为release-vx.y.z
- TenonOS发行版，格式为Tenon vx.y.z。用于Tenon仓库发行版命名、BSP仓库生成的TenonOS镜像包命名

Tenon与lib、app、driver等外部微库的版本配套关系维护在[BSP](board-support-package)仓库中，配置文件名格式为lib_config_Tenon_vx.y.z.json，其中Tenon_vx.y.z即TenonOS发行版名称。

## 版本管理文件格式说明
### 示例

该示例为 TenonOS v0.1.0 版本，TenonOS-v0.1.0 支持 helloworld 应用，树莓派平台，构建工具为 bsp-v0.1.0。

```
	{
        "apps": [
                {
                        "name": "app-helloworld",
                        "version": "release-v0.1.0",
                        "url": "https://gitee.com/tenonos-mirror/app-helloworld"
                }
        ],
        "plats": [
                {
                        "name": "plat-raspi",
                        "version": "release-v0.1.0",
                        "url": "https://gitee.com/tenonos/plat-raspi.git"
                }
        ],
        "tenon": {
                "version": "release-v0.1.0",
                "url": "https://gitee.com/tenonos/tenon.git"
        },
        "bsp": {
                "version": "release-v0.1.0",
                "url": "https://gitee.com/tenonos/board-support-package.git"
        }
}
```

### lib_config.json 详细解析

  - 微库配置
  支持的微库类型包括：apps、libs、drivers、plats，每个类型下可提供若干微库，格式如下：
  ```
  "微库类型": [
        {
            "name": "配置的微库名称(必填)",
            "version": "配置使用的版本号(选填)", 
            "branch": "配置使用的分支名称(选填), 当version与branch字段同时存在时version字段生效", 
            "url": "配置微库的git clone地址(必填)"
        }，
				{
						...
				},
  ],
  ```
  - 内核配置
  ```
  "tenon": {
          "version": "配置使用的版本号(选填)", 
          "branch": "配置使用的分支名称(选填), 当version与branch字段同时存在时version字段生效",
          "url": "配置Tenon的git clone地址(必填)"
  }
  ```
  - bsp配置
  ```
  "bsp": {
          "version": "配置使用的版本号(选填)", 
          "branch": "配置使用的分支名称(选填), 当version与branch字段同时存在时version字段生效",
          "url": "配置TenonOS Board Support Package的git clone地址(必填)"
  }
  ```
在初始化构建阶段，若指定 bsp.version, 则会对本地 bsp 版本进行版本校验，确保 bsp 版本为社区发布版本，且与在 lib_config.json 中指定版本一致。


[board-support-package]: https://gitee.com/tenonos/board-support-package/tree/master/version-management
