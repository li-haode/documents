## 组织介绍
TenonOS社区是一个开放、中立的操作系统开源社区，核心项目包括基于LibOS架构的操作系统Tenon和虚拟化平台Mortise。TenonOS突破了传统宏内核、微内核操作系统的模块边界和固有组成形式，基于微库解耦、灵活组合的设计理念，面向不同的软硬件环境提供动态、高效生成差异化操作系统实例的能力。

TenonOS具有LibOS解耦架构、面向场景生成、跨领域微库复用、POSIX接口兼容等主要特征，致力于降低操作系统开发、优化门槛，提升OS相关领域的生产效率。社区欢迎各类软硬件厂商、开发者和研究人员的参与，共同构建一个多元、包容的新型操作系统开源生态。

TenonOS社区包含以下主要项目和仓库，逐步开源中
|仓库| 说明 | 
|---|------|
|[Tenon][tenon-base]|基于libos架构的操作系统框架和核心微库|
|[Mortise][mortise-base]|基于libos架构的嵌入式虚拟化平台|
|[Roadmap][tenon-roadmap]|社区特性研发路标|
|[RequestForComments][tenon-rfcs]|大颗粒特性讨论、落地跟踪记录|
|[Documents][tenon-docs]|API、设计、博客等文档|
|plat-xxx|适配各类开发板的平台仓库|
|driver-xxx|驱动仓库|
|lib-xxx|各类功能微库|
|app-xxx|各类应用微库|

## 参与的组织或企业
浙江大学软件学院、浙江大学滨江研究院、杭州盈一科技有限公司、江南计算技术研究所

## 社区贡献
Tenon欢迎各方以个人、组织、企业形式参与到社区贡献当中，贡献指导请参考[Contributing][tenon-contributing]

## 首页信息
* [社区主页][tenon-community-mainpage]
* [微库中心][tenon-libcenter]

## 联系我们
网站: www.tenonos.org.cn
邮箱: community@yingyitech.net

[tenon-contributing]: https://gitee.com/tenonos/documents/CONTRIBUTING.md
[tenon-base]: https://gitee.com/tenonos/tenon
[mortise-base]: https://gitee.com/tenonos/mortise
[tenon-roadmap]: https://gitee.com/tenonos/roadmap
[tenon-docs]: https://gitee.com/tenonos/documents
[tenon-rfcs]: https://gitee.com/tenonos/request-for-comments
[tenon-libcenter]: https://www.tenonos.org.cn/libcenter
[tenon-community-mainpage]: https://www.tenonos.org.cn
